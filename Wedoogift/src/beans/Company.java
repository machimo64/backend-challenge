package beans;

import java.util.List;

import beans.factoryPattern.Deposits;

public class Company {

	private String name;
	
	private Integer balance;
	
	public Company(String name, Integer balance) {
		this.name = name;
		this.balance = balance;
	}

	public String getName() {
		return name;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}
	
	/**
	 * Check if the company can distribute a list of deposits which total amount should not surpass current company's balance.
	 * Calculates the sum of the deposits amount and compare it to the company's balance 
	 * @param user The user 
	 * @param deposits The user to whom the deposits will be distributed
	 * @return true if the company balance allow it, false otherwise.
	 *
	 * Example :
	 * 	if a company have 50 of current balance and want to distribute 2 deposits :
	 * 		Deposit1 which has an amount of 10
	 * 		Deposit2 which has an amount of 20
	 * 	this function return true because 50 >= 30.
	 */
	public boolean distribute( User user, List<Deposits> deposits) {
		
		Integer amount = 0;
		
		for(Deposits d : deposits) {
			
			amount += d.getAmount();
			
		}
		
		if(amount > this.getBalance()) {
			
			return false;
		
		} else {
			
			this.setBalance(this.getBalance() - amount);
			user.getDepositsList().addAll(deposits);
			return true;
		}
	
	}
	
	
}
