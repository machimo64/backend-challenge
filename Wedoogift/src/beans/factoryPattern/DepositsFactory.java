package beans.factoryPattern;

import java.time.LocalDate;

public class DepositsFactory {
	
	/**
	 * Creates a repository instance based on the specified type.
	 * @param type The deposit type to be created.
	 * @param amount The amount of the deposit.
	 * @param startDate The start date of the deposit.
	 * @return A repository instance corresponding to the specified type.
	 * @throws IllegalArgumentException If the specified deposit type is not valid.
	 *
	 * Example :
	 * To create a new meal deposit with an amount of 50 and a start date of 1 June 2022,
	 * call the function with the following parameters: type = "mealDeposit", amount = 50, startDate = LocalDate.of(2022, 6, 1)
	 * This function will then return an instance of the MealDeposit class.
	 */
	public Deposits createDepositsFactory(String type, Integer amount, LocalDate startDate) {
        if (type.equalsIgnoreCase("mealDeposit")) {
            return new MealDeposit(amount, startDate);
        } else if (type.equalsIgnoreCase("giftDeposit")) {
            return new GiftDeposit(amount, startDate);
        } else {
            throw new IllegalArgumentException("Invalid deposit card type: " + type);
        }
    }
}
