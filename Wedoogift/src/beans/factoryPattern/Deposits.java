package beans.factoryPattern;

import java.time.LocalDate;

public interface Deposits {
	
	Integer getAmount();
	LocalDate getStartDate();
	Boolean isExpired(LocalDate startDate);
}
