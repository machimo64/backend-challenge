package beans.factoryPattern;
import java.time.LocalDate;

public class MealDeposit implements Deposits {

	private Integer amount;
	private LocalDate startDate;

	public MealDeposit(Integer amount, LocalDate startDate) {
		
		this.amount = amount;
		this.startDate = startDate;
	}
	
	@Override
	public Integer getAmount() {
		return amount;
	}

	@Override
	public LocalDate getStartDate() {
		return startDate;
	}

	/**
	 * Check if a MealDeposit card has expired or not.
	 * A mealDeposit card expire expires at the end of February of the year following the distribution date 
	 * @param currentDate The currentDate to check if the card has expired
	 * @return true if the card has expired, false otherwise.
	 * @throws IllegalArgumentException if the currentDate is null.
	 *
	 * Example :
	 * if the start date is 2023-04-02 and the current date is 2023-05-14
	 * this function return false because the card is not expired.
	 */
	public Boolean isExpired(LocalDate currentDate) {
		if (currentDate == null) {
	        throw new IllegalArgumentException("Current date can not be null");
	    }

		return currentDate.isAfter(startDate.withMonth(2).withDayOfMonth(28).plusYears(1));
	}
}