package beans;
import java.time.LocalDate;
import java.util.ArrayList;

import beans.factoryPattern.Deposits;

public class User {
	
	private String name;
	
	private ArrayList<Deposits> deposits;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Deposits> getDepositsList() {
		return deposits;
	}

	public void setDepositsList(ArrayList<Deposits> deposits) {
		this.deposits = deposits;
	}
	
	
	/**
	 * Check User balance.
	 * Calculates the sum of the user's unexpired deposits 
	 * @param depositsList The user's deposits
	 * @return the user current balance.
	 *
	 * Example :
	 * 	if the user has 2 deposits :
	 * 		Deposit1 which has an amount of 10 and has not expired
	 * 		Deposit2 which has an amount of 20 and has expired
	 * 	this function return 10.
	 */
	public int checkBalance(ArrayList<Deposits> depositsList) {
		
		int balance = 0;
		
		if(depositsList.size() == 0) {
			return 0;
		} else {
			for(Deposits deposit : depositsList) {
				if(!deposit.isExpired(LocalDate.now())) {
					balance += deposit.getAmount();
				}
			}
		}
		return balance;
		
	}
	
	
	
}
