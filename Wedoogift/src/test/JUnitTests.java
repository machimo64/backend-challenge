package test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

import beans.Company;
import beans.User;
import beans.factoryPattern.Deposits;
import beans.factoryPattern.DepositsFactory;

public class JUnitTests {
	
	
	/**
     * Test case for the Distribute method of the Company class
     * It tests if the distribute method returns true when the company tries to distribute an amount of gift lower or equals than
     * her balance
     */
	@Test
	public void testDistributeAllow() {
		
		Company apple = new Company("Apple", 400);
		
		User user = new User();
		
		ArrayList<Deposits> deposits = new ArrayList<Deposits>();
		
		DepositsFactory factory = new DepositsFactory();
		
		Deposits deposit1 = factory.createDepositsFactory("giftDeposit", 100, LocalDate.now());
		deposits.add(deposit1);
		
		
		deposit1 = factory.createDepositsFactory("giftDeposit", 70, LocalDate.now());
		deposits.add(deposit1);
		
		deposit1 = factory.createDepositsFactory("mealDeposit", 120, LocalDate.now());
		deposits.add(deposit1);
		
		
		deposit1 = factory.createDepositsFactory("mealDeposit", 40, LocalDate.now());
		deposits.add(deposit1);
	  
		user.setDepositsList(deposits);
		
		//The distribute method should return true because the sum of deposits his lower than company's balance
		assertTrue("The apple balance allow to spend 370€", apple.distribute(user, deposits));
	}
	
	/**
     * Test case for the Distribute method of the Company class
     * It tests if the distribute method returns false when the company tries to distribute an amount of gift superior than
     * her balance
     */
	@Test
	public void testDistributeDontAllow() {
		
		Company apple = new Company("Apple", 300);
		
		User user = new User();
		
		ArrayList<Deposits> deposits = new ArrayList<Deposits>();
		
		DepositsFactory factory = new DepositsFactory();
		
		Deposits deposit1 = factory.createDepositsFactory("giftDeposit", 100, LocalDate.now());
		deposits.add(deposit1);
		
		
		deposit1 = factory.createDepositsFactory("giftDeposit", 70, LocalDate.now());
		deposits.add(deposit1);
		
		deposit1 = factory.createDepositsFactory("mealDeposit", 120, LocalDate.now());
		deposits.add(deposit1);
		
		
		deposit1 = factory.createDepositsFactory("mealDeposit", 40, LocalDate.now());
		deposits.add(deposit1);
	  
		user.setDepositsList(deposits);
		
		//The distribute method should return false because the sum of deposits his superior than company's balance
		assertFalse("The apple balance doesn't allow to spend 330€", apple.distribute(user, deposits));
		
	}
	
	/**
     * Test case for the isExpired method of the GiftDeposit class which implements Deposits interface
     * It tests if the isExpired method returns false when the giftDeposit has been created less than 365 days ago
     */
	@Test
	public void testGiftDepositIsNotExpired() {
		
		DepositsFactory factory = new DepositsFactory();
		Deposits giftDeposit = factory.createDepositsFactory("giftDeposit", 100, LocalDate.of(2022, 05, 24));
		
		//The isExpired method should return false because the giftDeposit has been created less than 365 days ago
		assertFalse("The end date is later than today", giftDeposit.isExpired(LocalDate.now()));
	}
	
	/**
     * Test case for the isExpired method of the GiftDeposit class which implements Deposits interface
     * It tests if the isExpired method returns true when the giftDeposit has been created more than 365 days ago
     */
	@Test
	public void testGiftDepositIsExpired() {
		
		DepositsFactory factory = new DepositsFactory();
		Deposits giftDeposit = factory.createDepositsFactory("giftDeposit", 100, LocalDate.of(2022, 04, 24));
		
		//The isExpired method should return false because the giftDeposit has been created more than 365 days ago
		assertTrue("The end date is later than today", giftDeposit.isExpired(LocalDate.now()));
	}
	
	/**
     * Test case for the isExpired method of the mealDeposit class which implements Deposits interface
     * It tests if the isExpired method returns false when the mealDeposit has been created during the current year 
     * (because it expires on February of next year)
     */
	@Test
	public void testmealDepositValidWhenCreatedThisYear() {
		
		
		DepositsFactory factory = new DepositsFactory();
		Deposits giftDeposit = factory.createDepositsFactory("mealDeposit", 100, LocalDate.of(2023, 01, 05));
		
		//The isExpired method should return false because the mealDeposit has been created on the current year
		assertFalse("The end date is on 28/02/2024", giftDeposit.isExpired(LocalDate.now()));
	}
	
	/**
     * Test case for the isExpired method of the mealDeposit class which implements Deposits interface
     * It tests if the isExpired method returns false when the mealDeposit has been created last year 
     * and the date is before 28th February of current year
     */
	@Test
	public void testmealDepositValidWhenCreatedLastYear() {
		
		
		DepositsFactory factory = new DepositsFactory();
		Deposits giftDeposit = factory.createDepositsFactory("mealDeposit", 100, LocalDate.of(2022, 01, 05));
		
		// The isExpired method should return false because the mealDeposit has been created last year
		// and the current date is before 28th february of this year
		assertFalse("The end date is on 28/02/2023", giftDeposit.isExpired(LocalDate.of(2023,02,25)));
	}
	
	/**
     * Test case for the isExpired method of the mealDeposit class which implements Deposits interface
     * It tests if the isExpired method returns false when the mealDeposit has been created during the last year 
     * and the current date is later than 28th February of the current year
     */
	@Test
	public void testmealDepositExpired() {
		
		
		DepositsFactory factory = new DepositsFactory();
		Deposits giftDeposit = factory.createDepositsFactory("mealDeposit", 100, LocalDate.of(2022, 12, 28));
		
		//The isExpired method should return false because the mealDeposit has been created last year
		assertTrue("The end date is on 28/02/2023", giftDeposit.isExpired(LocalDate.now()));
	}
	
	/**
     * Test case for the checkSolde method of the User class
     * It tests if the checkSolde method returns the good amount for all his deposits that have not expired  
     */
	@Test
	public void testUserBalance() {
		
		User user = new User();
		ArrayList<Deposits> deposits = new ArrayList<Deposits>();
		
		DepositsFactory factory = new DepositsFactory();
		
		Deposits deposit1 = factory.createDepositsFactory("giftDeposit", 100, LocalDate.of(2022, 06, 02));
		deposits.add(deposit1);
		
		Deposits deposit2 = factory.createDepositsFactory("giftDeposit", 80, LocalDate.of(2022, 04, 05));
		deposits.add(deposit2);
		
		Deposits deposit3 = factory.createDepositsFactory("mealDeposit", 120, LocalDate.of(2022, 02, 07));
		deposits.add(deposit3);
		
		Deposits deposit4 = factory.createDepositsFactory("mealDeposit", 140, LocalDate.of(2023, 02, 06));
		deposits.add(deposit4);
		
		user.setDepositsList(deposits);

		//The checkSolde method should return 240 because only deposit1 and deposit3 have not expired, and there total combined amount is 240
		assertEquals("The user balance should be equals to 240", user.checkBalance(deposits), 240);
		
		
	}
}
